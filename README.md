package listaSE;

/*
    LISTA SIMPLEMENTE ENLAZADA DE DATOS ENTEROS
*/
class CNodo {
	int dato;
	CNodo siguiente;
        
	public CNodo()	{
            siguiente = null;
	}
}

class CLista {
    
    CNodo cabeza;
    
    public CLista()
    {
            cabeza = null;
    }

    public void InsertarDato(int dat ) {
        CNodo NuevoNodo;
        CNodo antes, luego;//crea dos variable antes y luego 
        NuevoNodo = new CNodo();//new llama al constructor CNodo
        NuevoNodo.dato=dat;
        int ban=0;
        if (cabeza == null){ //lista esta vacia
            NuevoNodo.siguiente=null;//ya esta echo 
            cabeza = NuevoNodo;
        }
        else {
            if (dat<cabeza.dato) //dato va antes de cabeza
            {
                    NuevoNodo.siguiente=cabeza;
                    cabeza = NuevoNodo;
            }
                else {  //dato va despues de cabeza 
                        antes=cabeza;
                        luego=cabeza;
                        while (ban==0)
                        {
                            if (dat>=luego.dato) 
                            {
                                antes=luego;
                                luego=luego.siguiente;
                            }
                            if (luego==null)
                            {
                                ban=1;
                            }
                            else 
                            {
                                    if (dat<luego.dato){
                                        ban=1;
                                    }
                            }
                        }
                        antes.siguiente=NuevoNodo;
                        NuevoNodo.siguiente=luego;
                }
        }
    }

    public void EliminarDato(int dat) {
        CNodo antes,luego;
        int ban=0;
        if (Vacia()) {
            System.out.print("Lista vacía ");
        }
        else {  if (dat<cabeza.dato) {
                    System.out.print("dato no existe en la lista ");
                }
                else {
                        if (dat==cabeza.dato) {
                            cabeza=cabeza.siguiente;
                        }
                        else {  antes=cabeza;
                                luego=cabeza;
                                while (ban==0) {
                                    if (dat>luego.dato) {
                                        antes=luego;
                                        luego=luego.siguiente;
                                    }
                                    else ban=1;
                                    if (luego==null) {
                                        ban=1;
                                    }
                                    else {
                                            if (luego.dato==dat) 
                                                ban=1;
                                    }
                                }
                                if (luego==null) {
                                    System.out.print("dato no existe en la Lista ");
                                }
                                else {
                                        if (dat==luego.dato) {
                                            antes.siguiente=luego.siguiente;
                                        }
                                        else 
                                            System.out.print("dato no existe en la Lista ");
                                }
                        }
                }
        }
    }
//si esque cabeza es igual a null retorna a true 
    public boolean Vacia() {
        return(cabeza==null);
    }

    public void Imprimir() {
        CNodo Temporal;
        Temporal=cabeza;
        if (!Vacia()) {
            while (Temporal!=null) {
                System.out.print(" " + Temporal.dato +" ");
                Temporal = Temporal.siguiente;
            }
            System.out.println("");
        }
        else
            System.out.print("Lista vacía");
    }
}

public class ListaSEnlazada {
    public static void main(String args[]) {
        //creamos un objeto y el constructor asignado Null a cabeza 
        CLista objLista = new CLista();//cada vez que tenemos los () estamamos llamando la clase clista
        System.out.println("Lista Simplemente enlazada:");
        //ingresar un dato cuando la lista esta vacia 
        objLista.InsertarDato(9);//la lista tiene un elemento que es el nueve 
        objLista.InsertarDato(20);//Insertar un dato mayor a los que ya están en la lista
        objLista.InsertarDato(7);
        //objLista.InsertarDato(7);//Insertar un dato que ya está en la lista
        objLista.InsertarDato(10);
        objLista.Imprimir();
        System.out.println("\nLista Sin el dato a eliminar: ");
        objLista.EliminarDato(7);
        objLista.Imprimir();
    }
}
